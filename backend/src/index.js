const express = require('express'); //importa pacote
const cors = require('cors');
const routes = require('./routes'); //importa de arquivo

const app = express(); //criando a aplicacao

app.use(cors());
app.use(express.json());
app.use(routes);

app.listen(3333);