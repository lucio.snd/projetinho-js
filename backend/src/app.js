const express = require('express'); //importa pacote
const cors = require('cors');
const { errors } = require('celebrate');
const routes = require('./routes'); //importa de arquivo

const app = express(); //criando a aplicacao

app.use(cors());
app.use(express.json());
app.use(routes);
app.use(errors());

module.exports = app;