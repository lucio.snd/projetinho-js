import { StyleSheet } from 'react-native';
import Constants from 'expo-constants';

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 24,
    paddingTop: Constants.statusBarHeight + 20
  },

  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },

  headerText: {
    fontSize: 15,
    color: '#737380'
  },

  headerTextBold: {
    fontWeight: 'bold'
  },

  incident: {
    marginTop: 48,
    padding: 24,
    borderRadius: 8, 
    backgroundColor: '#fff',
    marginBottom: 16
  },

  incidentProperty: {
    fontSize: 14,
    color: '#41414d',
    fontWeight: 'bold'
  },

  incidentValue: {
    marginTop: 4,
    fontSize: 14, 
    marginBottom: 24,
    color: '#737380'
  },

  contactBox: {
    padding: 24,
    borderRadius: 8, 
    backgroundColor: '#fff',
    marginBottom: 16
  },

  heroTitle: {
    fontSize: 24,
    color: '#13131a',
    fontWeight: 'bold'
  },

  heroDescription: {
    marginTop: 24,
    fontSize: 18, 
    marginBottom: 24,
    color: '#737380'
  },

  actions: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },

  action: {    
    borderRadius: 8, 
    backgroundColor: '#e02041',
    height: 50,
    width: '48%',
    justifyContent: 'center',
    alignItems: 'center'
  },

  actionText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: 'bold'
  }

});